#!/bin/sh

if [ -n "$DESTDIR" ] ; then
    case $DESTDIR in
        /*) # ok
            ;;
        *)
            /bin/echo "DESTDIR argument must be absolute... "
            /bin/echo "otherwise python's distutils will bork things."
            exit 1
    esac
fi

echo_and_run() { echo "+ $@" ; "$@" ; }

echo_and_run cd "/home/leandre/Documents/icpr/icpr_robotsimu/src/pysdf"

# ensure that Python install destination exists
echo_and_run mkdir -p "$DESTDIR/home/leandre/Documents/icpr/icpr_robotsimu/install/lib/python2.7/dist-packages"

# Note that PYTHONPATH is pulled from the environment to support installing
# into one location when some dependencies were installed in another
# location, #123.
echo_and_run /usr/bin/env \
    PYTHONPATH="/home/leandre/Documents/icpr/icpr_robotsimu/install/lib/python2.7/dist-packages:/home/leandre/Documents/icpr/icpr_robotsimu/build/pysdf/lib/python2.7/dist-packages:$PYTHONPATH" \
    CATKIN_BINARY_DIR="/home/leandre/Documents/icpr/icpr_robotsimu/build/pysdf" \
    "/usr/bin/python2" \
    "/home/leandre/Documents/icpr/icpr_robotsimu/src/pysdf/setup.py" \
     \
    build --build-base "/home/leandre/Documents/icpr/icpr_robotsimu/build/pysdf" \
    install \
    --root="${DESTDIR-/}" \
    --install-layout=deb --prefix="/home/leandre/Documents/icpr/icpr_robotsimu/install" --install-scripts="/home/leandre/Documents/icpr/icpr_robotsimu/install/bin"
