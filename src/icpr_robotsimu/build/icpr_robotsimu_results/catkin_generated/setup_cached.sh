#!/usr/bin/env sh
# generated from catkin/python/catkin/environment_cache.py

# based on a snapshot of the environment before and after calling the setup script
# it emulates the modifications of the setup script without recurring computations

# new environment variables

# modified environment variables
export CMAKE_PREFIX_PATH="/home/leandre/Documents/icpr/icpr_robotsimu/devel/.private/icpr_robotsimu_results:$CMAKE_PREFIX_PATH"
export PWD='/home/leandre/Documents/icpr/icpr_robotsimu/build/icpr_robotsimu_results'
export ROSLISP_PACKAGE_DIRECTORIES="/home/leandre/Documents/icpr/icpr_robotsimu/devel/.private/icpr_robotsimu_results/share/common-lisp:$ROSLISP_PACKAGE_DIRECTORIES"
export ROS_PACKAGE_PATH="/home/leandre/Documents/icpr/icpr_robotsimu/src/icpr_robotsimu_results:$ROS_PACKAGE_PATH"