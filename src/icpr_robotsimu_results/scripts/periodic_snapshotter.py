#!/usr/bin/env python
import roslib
roslib.load_manifest('laser_assembler')
import rospy
from std_msgs.msg import String
from sensor_msgs.msg import PointCloud

import time
from laser_assembler.srv import *

rospy.init_node('periodic_snapshotter')
rospy.wait_for_service("assemble_scans")

while not rospy.is_shutdown():
    try:
        assemble_scans = rospy.ServiceProxy('assemble_scans', AssembleScans)
        nuage = assemble_scans(rospy.Time(0,0), rospy.get_rostime())
        pub = rospy.Publisher('cloud', PointCloud, queue_size=1)
        #print("------------Le type"+str(nuage.type()))
        rate = rospy.Rate(1)
        msg = AssembleScansResponse()
        msg = nuage.cloud
        pub.publish(msg)
        time.sleep(5)
    except rospy.ServiceException, e:
        print "Service call failed: %s"%e
