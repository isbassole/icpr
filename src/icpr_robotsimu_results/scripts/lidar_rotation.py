#!/usr/bin/env python

import rospy
from std_msgs.msg import Float64



rospy.init_node('lidar_rotation')
pub = rospy.Publisher('laser_velocity_controller/command', Float64, queue_size=1000)
rate = rospy.Rate(1000)
while not rospy.is_shutdown():
    msg = Float64()
    msg.data = 10
    pub.publish(msg)
    rate.sleep()
